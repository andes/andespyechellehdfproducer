# Scripts to produce the input HDF files for pyEchelle. 


These scripts are ports of the scripts from the original Echelle++

## Requirements
requirement for python are in the requirement.txt 


## Zemax input file.
In order to produce the model you need to a clean zemax files with the following properties:

- all apertures are removed but for the image surface where a rectangular aperture. 
- There should be only one configuration, field, wavelength (with working numbers)
- The Echelle, Gamma, and Blaze should be signalled with a single surface with the names in the comments. 

An example is given along the code.


## Usage :

The script to run is model_creation_ANDES_R.py or model_creation_ANDES_IZ.py
If one need to adapt it to a new spectrgraph one needs to copy and adapt one of these 2 files.

The parameter to adapts are:

* the minimum and maximum of the orders
* the CCD number of the pixes, and the pixel size
* the number and the position and the size of the fibers in the field
* the shape of the fibers

One important step is to check