import numpy as np
import pylab as plt
import h5py

res = h5py.File("ANDES_R01.hdf")
plt.figure(1)
for i in range(81,99):
        plt.plot(res["CCD_1"][f"fiber_{1}"][f"order{i}"]["translation_x"],res["CCD_1"][f"fiber_{1}"][f"order{i}"]["translation_y"],f"C{i%8}")
plt.plot([0,9000,9000,0,0],[0,0,9000,9000,0],"k")
plt.xlim(0,9000)
plt.ylim(3000,3500)
plt.axis("equal")
res.close()
plt.title("R arm")


res = h5py.File("ANDES_IZ01.hdf")
plt.figure(2)
for i in range(65,82):
        plt.plot(res["CCD_1"][f"fiber_{1}"][f"order{i}"]["translation_x"],res["CCD_1"][f"fiber_{1}"][f"order{i}"]["translation_y"],f"C{i%8}")
plt.plot([0,9000,9000,0,0],[0,0,9000,9000,0],"k")
plt.xlim(0,9000)
plt.ylim(3000,3500)
plt.axis("equal")
res.close()
plt.title("IZ arm")


plt.show()



