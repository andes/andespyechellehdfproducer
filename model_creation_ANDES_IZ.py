# this script shows how to generate a spectrograph model file from the Zemax model of a spectrograph using PyZDDE

# Zemax file has to be prepared in a certain way :
# 1 wavelength, 1 field
# All aperture removed and semi-diameter to automatic (there is probably a better way but this works)

import PyEchelle_Multimode
import pyzdde.zdde as pyz
import numpy as np
global _global_use_unicode_text
global _global_pyver3
_global_pyver3 = True
_global_use_unicode_text = False
import numpy
import sys
import logging
logging.basicConfig(format='%(asctime)s: %(message)s',level=logging.DEBUG)

import warnings
from tables import NaturalNameWarning
warnings.filterwarnings("ignore",category=NaturalNameWarning)

# Create pyzdde link to ZEMAX
ln = pyz.createLink()

# Create echelle spectrograph
spectrograph = PyEchelle_Multimode.Echelle(ln, 'ANDES_IZ01')

# Analyze ZEMAX file to extract e.g. blaze and gamma angles
spectrograph.analyseZemaxFile(echellename='Echelle', blazename='Blaze', gammaname='Gamma')

# Define minimum and maximum order
spectrograph.minord = 65
#spectrograph.minord = 101
#spectrograph.maxord = 102
spectrograph.maxord = 82
#spectrograph.maxord = 66

# Define CCD including pixel size
spectrograph.setCCD(PyEchelle_Multimode.CCD(9216, 9232, 10, name='CCD'))

#Here this is necessary to get the right wavlengh computation on the detector
ln.zSetAperture(58,4,46,46)
ln.zPushLens()

# Calculate wavelength bounds for all orders
spectrograph.calc_wl()

#the aperture of the image surface is increased to get unvignetted PSFS
ln.zSetAperture(58,4,70,70)
ln.zPushLens()

# Generate spectrograph model file for PyEchelle
filename = 'ANDES_IZ01.hdf'
PyEchelle_Multimode.save_CCD_info_to_hdf(filename, spectrograph.CCD)
PyEchelle_Multimode.save_spectrograph_info_to_hdf(filename, spectrograph)

# Fiber slit definition
fwidth = 0.630 # Fiber width in mm
nfiber = 66
yfield = (np.arange(nfiber)-nfiber/2)*fwidth # Vertical distribution of the fibers
xfield = np.zeros(nfiber) # Slit is vertical in its definition

#Loop iterating on the fibers to produce the affine transformation matrices and the PSFs.
for i in range(nfiber):
    #One sets the field in zemax for the current fiber
    try:
        ln.zSetField(1,xfield[i],yfield[i])
    except:
        pass

    try:
        if yfield[i]!=0:
            ln.zSetField(2,xfield[i],yfield[i]+numpy.sign(yfield[i])*fwidth/2)
        else:
            ln.zSetField(2,xfield[i],fwidth/2)
    except:
        pass

    ln.zPushLens()

    #compute zemax normalized fields for the current fibers
    if yfield[i] !=0:
        xn = xfield[i] / numpy.sqrt(xfield[i]**2+(yfield[i]+numpy.sign(yfield[i])*fwidth/2)**2)
        yn = yfield[i] / numpy.sqrt(xfield[i]**2+(yfield[i]+numpy.sign(yfield[i])*fwidth/2)**2)
        xd = fwidth/2  / numpy.sqrt(xfield[i]**2+(yfield[i]+numpy.sign(yfield[i])*fwidth/2)**2)

    else:
        xn=0
        yn=0
        xd=1

    logging.info("#############################")
    logging.info(f"Fiber #{i+1}/{nfiber} ")
    logging.info(f"Normalized fields,{xn},{yn},{xd}, {abs(yn)+xd}")
    logging.info("#############################")

    # Calculate affine transformation matrices across each order for fiber 1
    att = spectrograph.do_affine_transformation_calculation(15,norm_field=[[-xd, yn+xd], [-xd, yn-xd], [xd, yn-xd], [xd, yn+xd], [xn, yn]], fw=fwidth, fh=fwidth) #norm_field=[[xn, yn]]
    #print(att)
    PyEchelle_Multimode.save_transformation_to_hdf(filename, att, i+1,fiber_shape="circular")

    #Calculate PSFs across each order for fiber 1
    psf = spectrograph.get_psfs(15, 1, [xfield[i], yfield[i]])
    PyEchelle_Multimode.save_psfs_to_hdf(filename, psf, i+1)

# Close ZEMAX link


ln.close()
